#pragma once
#ifndef LEVEL_H
#define LEVEL_H
#include<vector>
#include<string>
#include"Player.h"
#include"Inventory.h"

class Level
{
public:
	Level(Player* playerParam);
	Level();
	void loadLevel(std::string fileName);
	char getMapObj(int x, int y) const;
	int getMapSize() const;
	void setMapObj(int x, int y, char c);
	void printMap();
	void addItemToMap(int _x, int _y, char _c);

private:
	Player* player;
	std::vector <std::string> levelMap;
};

#endif
