#pragma once
class Stats
{
public:
	Stats(float healthParam, int strParam, int _lvl, int _def);
	Stats();
	void setHealth(float healthParam);
	float getHealth() const;
	void setStr(int strParam);
	int getStr() const;
	void setDef(int defPram);
	int getDef() const;
	void setExp(float expParam);
	void takeDamage(float dmg);
	float getExp() const;
	void setLvl(int _lvl);
	int getLvl() const;
	void setMaxHealth(float max);
private:
	int strength;
	float health;
	float maxHealth;
	int defense;
	float expierience;
	int lvl;
};

