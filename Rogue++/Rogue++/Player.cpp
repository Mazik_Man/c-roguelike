#include "Player.h"

Player::Player(float healthParam, int strParam, int _lvl, int _def)
	:GameObject(healthParam, strParam, _lvl, _def)
{
	maxLvl = 10;
	currentLevel = _lvl;
	requiredExp = 100;
}

void Player::printStats()
{
	std::cout << "Name: " << getName() << "\tHealth: " << getHealth() << "\tDefense: " << getDef() << "\tLevel: " << getLvl()<< "\tExperience: " << getExp() << "/" << requiredExp << std::endl;
}

void Player::levelUp(float _exp)
{
	float expierience = getExp() + _exp;
	setExp(expierience);
	if (expierience >= requiredExp)
	{
		setLvl(getLvl() + 1);
		requiredExp *= 2.2;
		setMaxHealth(10);
		setStr(getStr() + 1);
		setDef(getDef() + 2);
	}
}
