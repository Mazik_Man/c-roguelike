#pragma once
#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H
#include"Player.h"
#include"Level.h"
#include"Enemy.h"
#include"NPC.h"

class Behaviour
{
public:
	Behaviour(Player* playerParam, Level* levelParam, NPC* npcParam);
	Behaviour(Player* playerParam, Enemy* enemyParam, Level* levelParam, NPC* npcParam);
	Behaviour();
	void movePlayer();
	void reLoad();
	void goTowardsPlayer();
	void pos(int x, int y);
	void fight();
private:
	Player* player;
	Enemy* enemy;
	NPC* npc;
	Level* level;
	bool notDone = false;
};

#endif