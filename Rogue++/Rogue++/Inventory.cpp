#include "Inventory.h"
#include<iostream>

void Inventory::addItemAfter(Items* position, Items* after)
{
	if (position != NULL)
	{
		Items* next = position->getNext();

		position->setNext(after);

		after->setNext(next);
		after->setPrevious(position);

		if (next != NULL)
		{
			next->setPrevious(after);
		}
	}
	else
		start = after;
}

void Inventory::insertItem(Items* startParam)
{
	if(start == NULL)
		start = startParam;
	else
	{
		Items* currentItem = start;

		while (currentItem != NULL)
		{
			if (currentItem->getNext() == NULL)
			{
				currentItem->setNext(startParam);
				startParam->setPrevious(currentItem);
				break;
			}
			currentItem = currentItem->getNext();
		}
	}
}

void Inventory::removeItem(Items* itemToRemove)
{
	Items* prev = itemToRemove->getPrevious();
	Items* next = itemToRemove->getNext();

	if (prev == NULL)
	{
		if (next != NULL)
			start = next;
		else
			start = NULL;
	}

	if (prev != NULL)
	{
		prev->setNext(next);
	}
	if (next != NULL)
	{
		next->setPrevious(prev);
	}

	itemToRemove->setNext(NULL);
	itemToRemove->setPrevious(NULL);
	//delete itemToRemove;
}

void Inventory::checkItems() const
{
	Items* currentCustomer = start;

	while (currentCustomer != NULL)
	{
		
	}
}

void Inventory::printItems(int x, int y) const
{
	COORD xy;
	xy.X = x;
	xy.Y = y;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE), xy);
	int count = 0;
	Items* currentItem= start;
	if (currentItem == NULL)
	{
		std::cout;
	}

	while (currentItem != NULL)
	{
		currentItem->setId(count);
		std::cout << count << ") " << "Name: " << currentItem->getName() << std::endl;
		count++;
		currentItem = currentItem->getNext();
	}
}

int Inventory::useItemInInventory(int _i)
{
	Items* currentItem = start;

	while (currentItem != NULL)
	{
		if (currentItem->getID() == _i)
		{
			int num = currentItem->useItem();
			removeItem(currentItem);
			return num;
		}
		currentItem = currentItem->getNext();
	}
	return 0;
}


