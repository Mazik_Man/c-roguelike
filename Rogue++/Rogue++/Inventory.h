#pragma once
#include"Items.h"
#include<Windows.h>

class Inventory
{
public:
	void addItemAfter(Items* position, Items* after);
	void insertItem(Items* startParam);
	void removeItem(Items* itemToRemove);
	void printItems(int x, int y) const;
	void checkItems() const;
	int useItemInInventory(int _i);
	Items* start;
private:
};

