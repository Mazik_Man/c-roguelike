#pragma once
#include<string>

class Items
{
public:
	Items();
	std::string getName();
	void setName(std::string nameparam);
	Items* getNext() const;
	Items* getPrevious() const;
	void setNext(Items* nextParam);
	void setPrevious(Items* previousParam);
	void removeItem();
	void setId(int idParam);
	int getID() const;
	virtual int useItem() { return 0; }
private:
	std::string name;
	int id;
	Items* next;
	Items* previous;
};

