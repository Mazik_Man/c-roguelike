#include "Behaviour.h"
#include <cstdlib>
#include"Player.h"
#include <time.h> 
#include <stdlib.h>   
#include <iostream>
#include <fstream>
#include <sstream>
#include"Items.h"
#include"HeathPotion.h"

Behaviour::Behaviour()
{
	player = NULL;
	level = NULL;
}

Behaviour::Behaviour(Player* playerParam, Level* levelParam, NPC* npcParam)
{
	player = playerParam;
	level = levelParam;
	npc = npcParam;
}

Behaviour::Behaviour(Player* playerParam, Enemy* enemyParam, Level* levelParamn, NPC* npcParam)
{
	player = playerParam;
	enemy = enemyParam;
	level = levelParamn;
	npc = npcParam;
}

void Behaviour::pos(int x, int y)
{
	COORD xy;
	xy.X = x;
	xy.Y = y;
	SetConsoleCursorPosition(
	GetStdHandle(STD_OUTPUT_HANDLE), xy);
}

void Behaviour::reLoad()
{
	COORD xy;
	xy.X = 0;
	xy.Y = level->getMapSize();
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE), xy);
	player->printStats();
	
}

void Behaviour::movePlayer()
{
	
 		int newPlayerPositionX = player->x;
		int newPlayerPositionY = player->y;
		int PlayerX;
		int PlayerY;
		player->getPoint(PlayerX,PlayerY);

		if (GetKeyState(VK_UP) & 0x8000)
		{

			//newPlayerPositionY = player->y - 1;
			if (level->getMapObj(PlayerX, PlayerY - 1) == '.')
			{
				newPlayerPositionY = player->y - 1;
			}
			else if (level->getMapObj(PlayerX, PlayerY - 1) == 'H')
			{
				Items* item = new HeathPotion();
				player->inv.insertItem(item);
				player->inv.printItems(0, level->getMapSize() + 4);
				level->setMapObj(PlayerX, PlayerY - 1, '.');
				newPlayerPositionY = player->y - 1;
			}
			else if (level->getMapObj(PlayerX, PlayerY - 1) == 'N')
			{
				pos(0, level->getMapSize() + 3);
				npc->quest();
				newPlayerPositionY = player->y;
			}
			else
			{
				newPlayerPositionY = player->y;
			}

		}

		if (GetKeyState(VK_DOWN) & 0x8000)
		{

			if (level->getMapObj(player->x, player->y + 1) == '.')
			{
				newPlayerPositionY = player->y + 1;
			}
			else if (level->getMapObj(player->x, player->y + 1) == 'H')
			{
				Items* item = new HeathPotion();
				player->inv.insertItem(item);
				player->inv.printItems(0, level->getMapSize() + 4);
				level->setMapObj(player->x, player->y + 1, '.');
				newPlayerPositionY = player->y + 1;
			}
			else if (level->getMapObj(player->x, player->y + 1) == 'N')
			{
				pos(0, level->getMapSize() + 3);
				npc->quest();
				newPlayerPositionY = player->y;
			}
			else
			{
				newPlayerPositionY = player->y;
			}
		}

		if (GetKeyState(VK_RIGHT) & 0x8000)
		{

			if (level->getMapObj(player->x + 1, player->y) == '.')
			{
				newPlayerPositionX = player->x + 1;
			}
			else if (level->getMapObj(player->x + 1, player->y) == 'H')
			{
				Items* item = new HeathPotion();
				player->inv.insertItem(item);
				player->inv.printItems(0, level->getMapSize() + 4);
				level->setMapObj(player->x + 1, player->y, '.');
				newPlayerPositionX = player->x + 1;
			}
			else if (level->getMapObj(player->x + 1, player->y) == 'N')
			{
				pos(0, level->getMapSize() + 3);
				npc->quest();
				newPlayerPositionX = player->x;
			}
			else
			{
				newPlayerPositionX = player->x;
			}
		}

		if (GetKeyState(VK_LEFT) & 0x8000)
		{

			if (level->getMapObj(player->x - 1, player->y) == '.')
			{
				newPlayerPositionX = player->x - 1;
			}
			else if(level->getMapObj(player->x - 1, player->y) == 'H')
			{ 
				Items* item = new HeathPotion();
				player->inv.insertItem(item);
				player->inv.printItems(0, level->getMapSize() + 4);
				level->setMapObj(player->x - 1, player->y, '.');
				newPlayerPositionX = player->x - 1;
			}
			else if (level->getMapObj(player->x - 1, player->y) == 'N')
			{
				pos(0, level->getMapSize() + 3);
				npc->quest();
				newPlayerPositionX = player->x;
			}
			else
			{
				newPlayerPositionX = player->x;
			}
		}
		if (GetKeyState('I') & 0x8000)
		{
			int i = 0;
			pos(0, level->getMapSize() + 2);
			std::cout << "Please enter an item number you want to use:";
			std::cin >> i;
			pos(0, level->getMapSize() + 2);
			for (int i = 0; i < 20; i++)
			{
				std::cout << "                                                                                                              " << std::endl;
			}
			player->setHealth(player->getHealth() + player->inv.useItemInInventory(i));
			reLoad();
			player->inv.printItems(0, level->getMapSize() + 4);
			
		}

		if (player->x >= 86 && player->y <= level->getMapSize() / 2 && !notDone)
		{
			pos(0, 0);
			level->loadLevel("Lp2.txt");
			level->printMap();
			notDone = true;
		}

		pos(player->x, player->y);
		std::cout << '.';

		pos(newPlayerPositionX, newPlayerPositionY);
		std::cout << player->getSprite();

		player->x = newPlayerPositionX;
		player->y = newPlayerPositionY;

		Sleep(60);
	
}



void Behaviour::fight()
{
	bool dead = false;
	if (enemy->getHealth() > 0)
	{
		srand(time(NULL));
		int playerDmg = rand() % player->getStr() + 1;
		int enemyDmg = rand() % enemy->getStr() + 1;

		COORD xy;
		xy.X = 0;
		xy.Y = level->getMapSize() + 2;
		SetConsoleCursorPosition(
			GetStdHandle(STD_OUTPUT_HANDLE), xy);

		enemy->takeDamage(playerDmg);
		std::cout << "Player hit enemy for " << playerDmg << " damage!";
		system("pause");

		xy.X = 0;
		xy.Y = level->getMapSize() + 2;
		SetConsoleCursorPosition(
			GetStdHandle(STD_OUTPUT_HANDLE), xy);

		player->takeDamage(enemyDmg);
		std::cout << "Enemy hit player for " << enemyDmg << " damage!";
		system("pause");

		reLoad();

		dead = true;
	}
	if (enemy->getHealth() <= 0 && dead)
	{
		COORD xy;
		xy.X = 0;
		xy.Y = level->getMapSize() + 2;
		SetConsoleCursorPosition(
			GetStdHandle(STD_OUTPUT_HANDLE), xy);
		std::cout << "Enemy DIED!";
		system("pause");
		npc->setEnemyCount(1);
		xy.X = enemy->x;
		xy.Y = enemy->y;
	
		int v1 = rand() % 4;
		SetConsoleCursorPosition(
			GetStdHandle(STD_OUTPUT_HANDLE), xy);
		if (v1 < 3)
		{
			std::cout << 'H';
			level->setMapObj(enemy->x, enemy->y, 'H');
		}
		else
		{
			std::cout << ".";
		}

		player->levelUp(50 * enemy->getLvl());
		
		reLoad();
	}
}

void Behaviour::goTowardsPlayer()
{
	if (enemy->getHealth() > 0)
	{
		int newEnemyPositionX = enemy->x;
		int newEnemyPositionY = enemy->y;
		int PlayerX;
		int PlayerY;
		player->getPoint(PlayerX, PlayerY);

		if (enemy->x > PlayerX)
		{
			newEnemyPositionX = enemy->x - 1;
		}
		else if (enemy->x < PlayerX)
		{
			newEnemyPositionX = enemy->x + 1;
		}


		if (enemy->y > PlayerY)
		{
			newEnemyPositionY = enemy->y - 1;
		}
		else if (enemy->y < PlayerY)
		{
			newEnemyPositionY = enemy->y + 1;
		}

		// Blank old enemy position
		pos(enemy->x, enemy->y);
		std::cout << '.';

		// Draw new enemy position
		pos(newEnemyPositionX, newEnemyPositionY);
		std::cout << enemy->getSprite();

		enemy->x = newEnemyPositionX;
		enemy->y = newEnemyPositionY;

		Sleep(60);
	}
}
