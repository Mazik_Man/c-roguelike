#include "GameManager.h"
#include"Behaviour.h"
#include"Level.h"
#include"Player.h"
#include<iostream>
#include<windows.h>
#include <math.h>
#include"Items.h"
#include"Inventory.h"
#include"HeathPotion.h"
#include"Enemy.h"

GameManager::GameManager()
{

}

void GameManager::LoadGame()
{
	Player p(20, 10, 1, 10);
	Level l(&p);
	Level lP2(&p);
	NPC npc(10, 10, 10, 10);
	Behaviour b(&p, &l, &npc);

	l.loadLevel("lel.txt");
	l.printMap();

	Enemy e(15, 10, 1, 1);
	Enemy e1(10, 10, 1, 1);
	Enemy e2(10, 10, 1, 1);
	Enemy e3(15, 10, 1, 1);
	Enemy e4(15, 10, 1, 1);

	Behaviour moveEnemy(&p, &e, &l, &npc);
	Behaviour moveEnemy1(&p, &e1, &l, &npc);
	Behaviour moveEnemy2(&p, &e2, &l, &npc);
	Behaviour moveEnemy3(&p, &e3, &l, &npc);
	Behaviour moveEnemy4(&p, &e4, &l, &npc);

	p.setPoint(3, 3);
	p.setSprite('m');
	p.printPlayer();

	npc.setPoint(10, 1);
	npc.setSprite('N');
	npc.printPlayer();

	l.setMapObj(10, 1, 'N');

	e.setPoint(140, 20);
	e.setSprite('G');
	e.printPlayer();

	e1.setPoint(130, 15);
	e1.setSprite('G');
	e1.printPlayer();

	e2.setPoint(92, 9);
	e2.setSprite('G');
	e2.printPlayer();

	e3.setPoint(15, 12);
	e3.setSprite('D');
	e3.printPlayer();

	e4.setPoint(15, 5);
	e4.setSprite('D');
	e4.printPlayer();

	bool done = true;
	while (npc.questDone())
	{
		b.movePlayer();
		npc.printPlayer();
		int dis = sqrt(pow(e.x - p.x, 2) + pow(e.y - p.y, 2));
		int dis2 = sqrt(pow(e1.x - p.x, 2) + pow(e1.y - p.y, 2));
		int dis3 = sqrt(pow(e2.x - p.x, 2) + pow(e2.y - p.y, 2));
		int dis4 = sqrt(pow(e3.x - p.x, 2) + pow(e3.y - p.y, 2));
		int dis5 = sqrt(pow(e4.x - p.x, 2) + pow(e4.y - p.y, 2));

		if (dis <= 5 && dis > 1)
		{
			moveEnemy.goTowardsPlayer();
		}
		if (dis == 1)
		{
			moveEnemy.fight();
		}

		if (dis2 <= 5 && dis2 > 1)
		{
			moveEnemy1.goTowardsPlayer();
		}
		if (dis2 == 1)
		{
			moveEnemy1.fight();
		}

		if (dis3 <= 5 && dis3 > 1)
		{
			moveEnemy2.goTowardsPlayer();
		}
		if (dis3 == 1)
		{
			moveEnemy2.fight();
		}

		if (dis4 <= 5 && dis4 > 1)
		{
			moveEnemy3.goTowardsPlayer();
		}
		if (dis4 == 1)
		{
			moveEnemy3.fight();
		}

		if (dis5 <= 5 && dis5 > 1)
		{
			moveEnemy4.goTowardsPlayer();
		}
		if (dis5 == 1)
		{
			moveEnemy4.fight();
		}

		if (e.getHealth()> 0)
			e.printPlayer();
		if (e2.getHealth()> 0)
			e2.printPlayer();
		if (e1.getHealth()> 0)
			e1.printPlayer();
		if (e3.getHealth()> 0)
			e3.printPlayer();
		if (e4.getHealth()> 0)
			e4.printPlayer();
	}

	system("pause");
}
