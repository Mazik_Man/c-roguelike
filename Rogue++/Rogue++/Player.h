#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "GameObject.h"
#include "Inventory.h"

class Player : public GameObject
{
public:
	Player(float healthParam, int strParam, int _lvl, int _def);
	void printStats();
	void levelUp(float _exp);
	Inventory inv;
private:
	int maxLvl = 10;
	int currentLevel = 1;
	float requiredExp;
};

#endif

