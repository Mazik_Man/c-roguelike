#include "Stats.h"
#ifndef STATS_H
#define STATS_H

Stats::Stats()
{
	health = 10;
}

Stats::Stats(float healthParam, int strParam, int _lvl, int _def)
{
	maxHealth = healthParam;
	health = maxHealth;
	strength = strParam;
	expierience = 0;
	defense = _def;
	lvl = _lvl;
}

void Stats::setHealth(float healthParam)
{
	if (health + healthParam > maxHealth)
	{
		health = maxHealth;
	}
	else
		health = healthParam;
}

float Stats::getHealth() const
{
	return health;
}

void Stats::setStr(int strParam)
{
	strength = strParam;
}

int Stats::getStr() const
{
	return strength;
}

void Stats::setDef(int defPram)
{
	defense = defPram;
}

int Stats::getDef() const
{
	return defense;
}

void Stats::setExp(float expParam)
{
	expierience = expParam;
}

float Stats::getExp() const
{
	return expierience;
}

void Stats::takeDamage(float dmg)
{
	health = health - ((dmg / defense) * 8);
	setHealth(health);
}

void Stats::setLvl(int _lvl)
{
	lvl = _lvl;
}

int Stats::getLvl() const
{
	return lvl;
}

void Stats::setMaxHealth(float max)
{
	maxHealth += max;
}
#endif // !