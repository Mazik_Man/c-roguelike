#pragma once
#include"Items.h"

class HeathPotion: public Items
{
public:
	HeathPotion();
	int getHealth() const;
	void setHealth(int healthParam);
	int useItem();
private:
	int health;
};

