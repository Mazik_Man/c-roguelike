#include "Level.h"
#include <fstream>
#include <iostream>

Level::Level()
{
	player = NULL;
}

Level::Level(Player* playerParam)
{
	player = playerParam;
}

void Level::loadLevel(std::string fileName)
{
	levelMap.clear();
	std::ifstream File;

	File.open(fileName);

	if (File.fail())
	{
		std::cout << "File Not Found" << std::endl;
	}
	
	std::string Line;

	while (getline(File, Line))
	{
		levelMap.push_back(Line);
	}
}

void Level::printMap()
{
	for (int i = 0; i < levelMap.size(); i++)
	{
		std::cout << levelMap[i] << std::endl;
	}
	player->printStats();
}

char Level::getMapObj(int x, int y) const
{
	return levelMap[y][x];
}

void Level::setMapObj(int x, int y, char c)
{
	levelMap[y][x] = c;
}

int Level::getMapSize() const
{
	return levelMap.size();
}


