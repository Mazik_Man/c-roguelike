#include "HeathPotion.h"

HeathPotion::HeathPotion()
{
	health = 100;
	setName("HealthPotion");
}


int HeathPotion::getHealth() const
{
	return health;
}

void HeathPotion::setHealth(int healthParam)
{
	health = healthParam;
}

int HeathPotion::useItem()
{
	return 30;
}
