#pragma once
#include "GameObject.h"

class NPC : public GameObject
{
public:
	NPC(float healthParam, int strParam, int _lvl, int _def);
	void printStats();
	void quest();
	bool questDone();
	void setEnemyCount(int count);
private:
	int enemyCount = 0;
};


