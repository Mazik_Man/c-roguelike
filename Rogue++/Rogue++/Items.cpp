#include "Items.h"



Items::Items()
{

}

void Items::setName(std::string nameparam)
{
	name = nameparam;
}


std::string Items::getName()
{
	return name;
}

Items* Items::getNext() const
{
	return next;
}

Items* Items::getPrevious() const
{
	return previous;
}

void Items::setNext(Items* nextParam)
{
	next = nextParam;
}

void Items::setId(int idParam)
{
	id = idParam;
}

void Items::setPrevious(Items* previousParam)
{
	previous = previousParam;
}

void Items::removeItem()
{
	Items* _prev = previous;
	Items* _next = next;

	if (_prev != NULL)
	{
		_prev->setNext(next);
	}
	if (_next != NULL)
	{
		_next->setPrevious(previous);
	}

	setNext(NULL);
	setPrevious(NULL);
}

int Items::getID() const
{
	return id;
}

