#include "NPC.h"

NPC::NPC(float healthParam, int strParam, int _lvl, int _def)
	:GameObject(healthParam, strParam, _lvl, _def)
{
	enemyCount = 0;
}

void NPC::printStats()
{

}

void NPC::quest()
{
	std::cout << "Kill 5 enemies and reward will be yours!" << std::endl;
}

bool NPC::questDone()
{
	if (enemyCount >= 5)
	{
		system("cls");
		std::cout << "You Completed your quest and now you are the king!" << std::endl;
		std::cout << "GameOver!" << std::endl;
		return false;
	}
	else
		return true;
}

void NPC::setEnemyCount(int count)
{
	enemyCount += count;
}





