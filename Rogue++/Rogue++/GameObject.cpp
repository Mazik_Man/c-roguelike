#include "GameObject.h"

GameObject::GameObject(float healthParam, int strParam, int _lvl, int _def)
	:Stats(healthParam, strParam, _lvl, _def)
{
	x = 0;
	y = 0;
	sprite = '@';
	name = "Mazik";
}

void GameObject::setPoint(int xParam, int yParam)
{
	x = xParam;
	y = yParam;
}

void GameObject::getPoint(int& X, int& Y)
{
	X = x;
	Y = y;
}

void GameObject::setSprite(char spriteParam)
{
	sprite = spriteParam;
}

char GameObject::getSprite()const
{
	return sprite;
}

void GameObject::printPlayer()
{
	COORD xy;
	xy.X = x;
	xy.Y = y;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE), xy);
	std::cout << sprite;
}

void GameObject::setName(std::string nameParam)
{
	name = nameParam;
}

std::string GameObject::getName() const
{
	return name;
}
