#pragma once
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Stats.h"
#include<iostream>
#include<string>
#include <windows.h>

class GameObject : public Stats
{
public:
	GameObject(float healthParam, int strParam, int _lvl, int _def);
	void setPoint(int xParam, int yParam);
	void getPoint(int& X, int& Y);
	virtual void printStats() = 0;
	void setSprite(char spriteParam);
	void setName(std::string nameParam);
	char getSprite()const;
	std::string getName() const;
	void printPlayer();
	int x;
	int y;
private:
	char sprite;
	std::string name;
};

#endif
